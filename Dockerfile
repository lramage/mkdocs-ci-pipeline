FROM python:alpine

RUN apk update && \
    apk upgrade && \
    apk add gcc \
            libffi-dev \
            make \
            musl-dev \
            openssl-dev \
            py3-pip \
            py3-setuptools \
            python3 \
            python3-dev && \
    python3 -m pip install --upgrade pip

WORKDIR /usr/src/mkdocs-ci-pipeline
COPY . /usr/src/mkdocs-ci-pipeline

RUN pip install -r requirements.txt

WORKDIR /srv/mkdocs
ENTRYPOINT ["/bin/sh", "/usr/src/mkdocs-ci-pipeline/entrypoint.sh"]
